﻿using UnityEngine;
using System.Collections;

public class grid : MonoBehaviour
{
    public GameObject tilePrefab;
    public int numberOfTiles = 100;
    public float distanceBetweenTiles = 14.5f;
    public int tilesPerRow = 10;

    void Start()
    {
        CreateTiles();
    }

    void CreateTiles()
    {
        float xOffset = 0.0f;
        float zOffset = 0.0f;
        float yOffset = 0.0f;
        for (int tileCreateZ = 0; tileCreateZ < 10; tileCreateZ++)
        {
            xOffset = 0.0f;
            yOffset = 0.0f;
            for (int tilesCreated = 0; tilesCreated < numberOfTiles; tilesCreated += 1)
            {
                xOffset += distanceBetweenTiles;
                if (tilesCreated % tilesPerRow == 0)
                {
                    yOffset += distanceBetweenTiles;
                    xOffset = 0;
                }
                Vector3 coords = new Vector3(transform.position.x + xOffset + distanceBetweenTiles, transform.position.y + yOffset - 30 + distanceBetweenTiles, transform.position.z + zOffset + distanceBetweenTiles);
                Instantiate(tilePrefab, coords, transform.rotation);
            }
            zOffset += distanceBetweenTiles;
        }
    }
}